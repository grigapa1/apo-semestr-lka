/*******************************************************************
  Project main function template for MicroZed based MZ_APO board
  designed by Petr Porazil at PiKRON

  main.c      - main file

  Pavel Grigar

 *******************************************************************/

#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <threads.h>
#include <string.h>
#include <stdbool.h>

#include "snake.h"
#include "write.h"
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"

unsigned char* parlcd_mem_base;
font_descriptor_t* font14x16 = &font_winFreeSystem14x16;

void mainMenu();

// MAIN FUNCTION, ALLOCATES MEMORY FOR THE GAME STATE AND CALLS THE GAME LOOP FUNCTION, ALSO MEASURES TIME PLAYED
int main(int argc, char *argv[])
{
    //INITIALIZE
    const int width = 480;
    const int height = 320;
    parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    unsigned int gameState[height * width]; // GAME BOARD

    //RUN
    mainMenu(gameState);

    //END PROGRAM
    printf("Game Over.\n");
    sleep(1);
    return 0;
}

void mainMenu(unsigned int gameState[]) {
    printf("Entering Main Menu.\n");
    uint16_t* fb;
    int speed = 2;
    fb = (uint16_t*)malloc(480 * 320 * sizeof(uint16_t));
    char input;
    bool running = true;
    clock_t t;

    // DRAW SNAKE LOGO IN FONT
    char str1[] = "SNAKES";
    draw_text(20, 120, 8, 8, 0xFFFF, font14x16, str1, parlcd_mem_base, fb);
    printf("Press any key to open menu.\n");
    getchar();
    for (int i = 0; i < 320 * 480; i++) { // CLEAR
        fb[i] = 0;
    }

    //PRINT MENU AND GET INPUT
    char str2[] = "PLAY - P";
    draw_text(40, 40, 5, 5, 0xFFFF, font14x16, str2, parlcd_mem_base, fb);
    char str3[] = "AI MODE - A";
    draw_text(40, 140, 5, 5, 0xFFFF, font14x16, str3, parlcd_mem_base, fb);
    char str4[] = "SPEED - +/-";
    draw_text(40, 240, 5, 5, 0xFFFF, font14x16, str4, parlcd_mem_base, fb);

    while (running) {
        scanf(" %c", &input);
        if (input == 'P') {
            t = clock();
            snake(gameState, false, speed);
            t = clock() - t;
            double time_taken = ((double)t) / CLOCKS_PER_SEC;
            printf("GAME TIME : %f seconds\n", time_taken);
            running = false;
        }
        else if (input == 'A') {
            t = clock();
            snake(gameState, true, speed);
            t = clock() - t;
            double time_taken = ((double)t) / CLOCKS_PER_SEC;
            printf("GAME TIME : %f seconds\n", time_taken);
            running = false;
        }
        else if (input == '+') {
            speed++;
            if (speed > 3) {
                speed = 3;
                printf("Already at maximum speed.\n");
            }
            printf("Current speed = %d.\n", speed);
        }
        else if (input == '-') {
            speed--;
            if (speed < 2) {
                speed = 2;
                printf("Already at minimum speed.\n");
            }
            printf("Current speed = %d.\n", speed);
        }
        else {
            printf("Operation not recognized, try again.\n");
        }
    }
    free(fb);
}

