**Autor:** Pavel Grigar  
**Projekt:** Hra Hadi  
**Předmět:** B35APO - Architektura počítačů  
**Semestr:** B192 - letní 2019/2020  
           

Úvod
====

Tento projekt vznikl v rámci semestrální práce na předmět APO. Jeho
cílem bylo vytvořit hru, ve které spolu soupeří dva hadi ve sběru drahokamů. Druhý had je vždy automatický, první může být ovládán přes SSH na klávesnici. Stav hadů pomáhají určit LEDky 1 a 2, každá určuje stav jednoho hada - zelená pokud se nic neděje, modrá pokud našel drahokam, červená pokud narazil a zemřel. Řada LED pod obrazovkou zaznamenává stav nejdelšího hada - pokud se takovýto had prodlouží, posune se o jedna doleva.


Instalace
=========

K provedení instalace bude potřeba stáhnout zdrojový kód z GITu,
zkompilovat ho a spustit na MZ_APO.  

#### Kroky instalace:

1.  Otevřít konzoli v GIT klientovi

2.  Naklonovat projekt příkazem: git clone
    git@gitlab.fel.cvut.cz:grigapa1/apo-semestr-lka.git

3.  Ve složce projektu (APO) zkompilovat projekt příkazem:  
    make TARGET  _IP=192.168.202.[poslední trojčíslí IP desky] run  

Ovládání
========

Hra se spustí a vypíše na obrazovku jméno hry, po zmáčknutí libovolné klávesy se dostaneme do jednoduchého menu. Zde můžeme volit mezi hrou jednoho hráče proti počítači, hrou dvou počítačů a upravovat v jistých mezích rychlost.

Jakmile se hra zapne v režimu hráče, je možné prvního hada, který se zobrazí uprostřed obrazovky, ovládal přes klávesy WSAD. Pokud příkaz neukazuje na druhou stranu než se had pohybuje, tak změní směr. Druhý had bude ovládán počítačem a se specifikovaným pořadím priorit následovat drahokamy.

### AI mód

Pokud zvolíme AI mód v menu, hra bude hrát sama proti sobě. První had má jinak nastavenou logiku takže se nebudou pohybovat úplně stejně.

#### Ukončení

Aplikaci lze kdykoliv za běhu ukončit z terminálu zmáčknutím CTR+C, případně se hra ukončí pokud jeden z hadů narazí do stěny, sebe sama nebo do druhého hada. Had který je po ukončení zelený vyhrál. Na ukončení reaguje i řada LED.

# Diagram

![Diagram](https://gitlab.fel.cvut.cz/grigapa1/apo-semestr-lka/-/raw/master/snakes.png)

