#pragma once

//#include <conio.h>
#include<time.h>
#include<stdint.h>

#define SPILED_REG_LED_RGB1_o           0x010


void init(unsigned int gameState[], int X[], int Y[], int XX[], int YY[], int l1, int l2, int rx, int ry);


int collision(int X[], int Y[], int XX[], int YY[], int l1, int l2);

void place(unsigned int gameState[], int position, uint16_t color);

void snake(unsigned int gameState[], bool ai, int s);
