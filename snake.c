#include <time.h>
#include <stdbool.h>
#include <stdio.h> 
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include "snake.h"
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "keyboard.h"

const int WIDTH = 480;
const int HEIGHT = 320;

void place(unsigned int gameState[], int position, uint16_t color) {
    int min = 0;
    int max = WIDTH * HEIGHT;
    gameState[position] = color;
    if (position + 1 <= max) {
        gameState[position + 1] = color;
    }
    if (position - 1 >= min) {
        gameState[position - 1] = color;
    }
    if (position + WIDTH <= max) {
        gameState[position + WIDTH] = color;
    }
    if (position - WIDTH >= min) {
        gameState[position - WIDTH] = color;
    }
    if (position + WIDTH + 1 <= max) {
        gameState[position + WIDTH + 1] = color;
    }
    if (position - WIDTH - 1 >= min) {
        gameState[position - WIDTH - 1] = color;
        gameState[position + 1 - WIDTH] = color;
        gameState[position - 1 + WIDTH] = color;
    }
}

void init(unsigned int gameState[], int X[], int Y[], int XX[], int YY[], int l1, int l2, int rx, int ry) { //CREATE BORDERS OF GAME
    for (int i = 0; i < HEIGHT; i++) {
        for (int j = 0; j < WIDTH; j++) {
            if (i == 0 || j == 0 || i == HEIGHT || j == WIDTH || i == 1 || j == 1 || i == HEIGHT - 1 || j == WIDTH - 1) {
                gameState[i * 480 + j] = 0xFFFF;
            }
        }
    }
    X[0] = WIDTH / 2;
    Y[0] = HEIGHT / 2;
    XX[0] = WIDTH / 2 + 20;
    YY[0] = HEIGHT / 2 + 20;
    for (int i = 1; i < l1; i++) { // SET FIRST POSITION OF SNAKE
        X[i] = X[0] - (i);
        Y[i] = Y[0];
        place(gameState, X[i] + Y[i] * WIDTH, 0x07E0);
    }
    for (int i = 1; i < l2; i++) { // SET FIRST POSITION OF AI SNAKE
        XX[i] = XX[0] - (i);
        YY[i] = YY[0];
        place(gameState, XX[i] + YY[i] * WIDTH, 0x07FF);
    }
    while (gameState[rx + ry * WIDTH] != 0) { // PLACE FIRST GEM
        rx = (1 + rand() % (WIDTH - 2));
        ry = (1 + rand() % (HEIGHT - 2));
    }
    place(gameState, rx + ry * WIDTH, 0xFFE0);
}

int collision(int X[], int Y[], int XX[], int YY[], int l1, int l2) {
    int result = 0;
    for (int i = 1; i < l1; i++) { // HIT ITSELF
        if (X[0] == X[i] && Y[0] == Y[i]) {
            result = 1;
            break;
        }
    }
    for (int i = 0; i < l2; i++) { // HIT SNAKE 2
        if (X[0] == XX[i] && Y[0] == YY[i]) {
            result = 1;
            break;
        }
    }
    for (int i = 1; i < l2; i++) { // HIT ITSELF
        if (XX[0] == XX[i] && YY[0] == YY[i]) {
            result += 2;
            break;
        }
    }
    for (int i = 0; i < l1; i++) { // HIT SNAKE 1
        if (XX[0] == X[i] && YY[0] == Y[i]) {
            result += 2;
            break;
        }
    }
    return result;
}

void snake(unsigned int gameState[], bool ai, int s) {
    unsigned char* mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    unsigned char* parlcd_mem_base;
    parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    parlcd_hx8357_init(parlcd_mem_base);
    uint32_t val_line = 6;
    int l = 6, l2 = 6, c = 0, d = 2, d2 = 2, rx = 100, ry = 100; //length, speed, collision, direction, gem coordinates
    int X[100], Y[100], XX[100], YY[100]; // MEMORY OF SNAKES on XY axis

    init(gameState, X, Y, XX, YY, l, l2, rx, ry); // INITIALIZE
    while (1) { // MAIN GAME LOOP
        //SET COLOR OF LED TO GREEN
        *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = 0x00FF00;
        *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = 0x00FF00;
        place(gameState, X[l - 1] + Y[l - 1] * WIDTH, 0); // MOVE TAIL
        place(gameState, XX[l2 - 1] + YY[l2 - 1] * WIDTH, 0); // MOVE TAIL
        for (int i = l - 1; i > 0; i--) { // UPDATE WHERE EACH PART OF SNAKE IS
            X[i] = X[i - 1];
            Y[i] = Y[i - 1];
        }
        for (int i = l2 - 1; i > 0; i--) { // UPDATE WHERE EACH PART OF SNAKE IS
            XX[i] = XX[i - 1];
            YY[i] = YY[i - 1];
        }
        //MOVE BASED ON CURRENT DIRECTION
        if (d == 0) X[0] = X[0] - s; // SNAKE 1
        if (d == 1) Y[0] = Y[0] - s;
        else if (d == 2) X[0] = X[0] + s;
        else if (d == 3) Y[0] = Y[0] + s;
        if (d2 == 0) XX[0] = XX[0] - s; // SNAKE 2
        if (d2 == 1) YY[0] = YY[0] - s;
        else if (d2 == 2) XX[0] = XX[0] + s;
        else if (d2 == 3) YY[0] = YY[0] + s;
        c = collision(X, Y, XX, YY, l, l2);
        if (c >= 3) { // HEAD ON HEAD COLLISION
            *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = 0xFF0000; //SET COLOR OF LED1 TO RED
            *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = 0xFF0000; //SET COLOR OF LED2 TO RED
            *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = 0xFFFFFFFF;
            return;
        }
        if (c==1 || gameState[Y[0] * 480 + X[0]] == 0xFFFF || gameState[Y[1] * 480 + X[1]] == 0xFFFF || gameState[Y[2] * 480 + X[2]] == 0xFFFF) {
            *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = 0xFF0000; //SET COLOR OF LED1 TO RED
            *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = 0xFFFFFFFF;
            return;//SNAKE 1 HIT SOMETHING AND DIED
        }
        if (c==2 || gameState[YY[0] * 480 + XX[0]] == 0xFFFF || gameState[YY[1] * 480 + XX[1]] == 0xFFFF || gameState[YY[2] * 480 + XX[2]] == 0xFFFF) {
            *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = 0xFF0000; //SET COLOR OF LED2 TO RED
            *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = 0xFFFFFFFF;
            return;//SNAKE 2 HIT SOMETHING AND DIED
        }
        //GET INPUT AND CHANGE DIRECTION IF POSSIBLE
        if (!ai) {
            int c;
            setMode(1);
            if ((c = getKey())) { //PLAYER DECIDES
                if ((c == 'd') && (d != 0)){
                d = 2;
                } else if((c == 'w') && (d != 3)){
                    d = 1;
                }else if ((c == 'a') && (d != 2)) {
                    d = 0;
                }else if ((c == 's') && (d != 1)) {
                    d = 3;
                }
            }
        } else { //OR AI DECIDES
            if ((X[0] < rx) && (d != 0)) {
                d = 2;
            }else if ((Y[0] > ry) && (d != 3)) {
                d = 1;
            }else if ((X[0] > rx) && (d != 2)) {
                d = 0;
            }else if ((Y[0] < ry) && (d != 1)) {
                d = 3;
            }   
        }
        // SNAKE 2 ALWAYS AI, DIFFERENT MOVEMENT PRIORITY SO THAT THEY DONT MOVE EXACTLY THE SAME
        if ((XX[0] > rx) && (d2 != 2)) d2 = 0;
        else if ((YY[0] < ry) && (d2 != 1)) d2 = 3;
        else if ((XX[0] < rx) && (d2 != 0)) d2 = 2;
        else if ((YY[0] > ry) && (d2 != 3)) d2 = 1;
        
        if (gameState[X[0] + Y[0] * WIDTH] == 0xFFE0 || gameState[Y[1] * 480 + X[1]] == 0xFFE0) // SNAKE 1 COUGHT A GEM
        {
            //SET COLOR OF LED1 TO BLUE
            *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = 0x0000FF;
            do {
                rx = (1 + rand() % (WIDTH - 2));
                ry = (1 + rand() % (HEIGHT - 2));
            } while(gameState[rx + ry * WIDTH] != 0); // FIND NEW SPACE FOR GEM
            for (int i = 0; i < HEIGHT * WIDTH; i++) {
                if (gameState[i] == 0xFFE0)
                    gameState[i] = 0;
            }
            place(gameState, rx + ry * WIDTH, 0xFFE0); // PLACE NEW GEM
            l = l + 1; // INCREASE LENGTH OF SNAKE
            if (d == 0 || d == 2) {
                X[l - 1] = X[l - 2] + (X[l - 2] - X[l - 3]);
                Y[l - 1] = Y[l - 2];
            }
            else {
                X[l - 1] = X[l - 2];
                Y[l - 1] = Y[l - 2] + (Y[l - 2] - Y[l - 3]);
            }
            if (l > l2) { // KEEP TRACK OF LONGEST SNAKE BY LEDS
                *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = val_line;
                val_line <<= 1;
            }
        }
        if (gameState[XX[0] + YY[0] * WIDTH] == 0xFFE0 || gameState[YY[1] * 480 + XX[1]] == 0xFFE0) // SNAKE 2 COUGHT A GEM
        {
            //SET COLOR OF LED2 TO BLUE
            *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = 0x0000FF;
            do {
                rx = (1 + rand() % (WIDTH - 2));
                ry = (1 + rand() % (HEIGHT - 2));
            } while(gameState[rx + ry * WIDTH] != 0); // FIND NEW SPACE FOR GEM
            for (int i = 0; i < HEIGHT * WIDTH; i++) {
                if (gameState[i] == 0xFFE0)
                    gameState[i] = 0;
            }
            place(gameState, rx + ry * WIDTH, 0xFFE0); // PLACE NEW GEM
            l2 = l2 + 1; // INCREASE LENGTH OF SNAKE
            if (d2 == 0 || d2 == 2) {
                XX[l - 1] = XX[l - 2] + (XX[l - 2] - XX[l - 3]);
                YY[l - 1] = YY[l - 2];
            }
            else {
                XX[l - 1] = XX[l - 2];
                YY[l - 1] = YY[l - 2] + (YY[l - 2] - YY[l - 3]);
            }
            if (l2 > l) { // KEEP TRACK OF LONGEST SNAKE BY LEDS
                *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = val_line;
                val_line <<= 1;
            }
        }
        for (int i = 0; i < l; i++) // DRAW SNAKE
            place(gameState, X[i] + Y[i] * WIDTH, 0x07E0);
        for (int i = 0; i < l2; i++) // DRAW SNAKE 2
            place(gameState, XX[i] + YY[i] * WIDTH, 0x07E0);
        drawdisplay(parlcd_mem_base, gameState);  //redraw LCD
        sleep(0.9); // SMALL DELAY BEFORE LOOPING AGAIN
    }
    return;
}


