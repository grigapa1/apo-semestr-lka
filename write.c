#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"


void draw_pixel(int x, int y, uint16_t color, uint16_t* fb) {
    if (x >= 0 && x < 480 && y >= 0 && y < 320) {
        fb[x + 480 * y] = color;
    }
}

void draw_pixel_height(int x, int y, uint16_t color, uint16_t* fb) {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            draw_pixel(x + i, y + j, color, fb);
        }
    }
}

int char_width(font_descriptor_t* fdes, int ch) {
    int width = 0;
    if ((ch >= fdes->firstchar) && (ch - fdes->firstchar < fdes->size)) {
        ch -= fdes->firstchar;
        if (!fdes->width) {
            width = fdes->maxwidth;
        }
        else {
            width = fdes->width[ch];
        }
    }
    return width;
}

void draw_char(int x, int y, int scale_y, font_descriptor_t* fdes, char ch, uint16_t color, uint16_t* fb) {
    int w = char_width(fdes, ch);
    if (w > 0) {
        const font_bits_t* ptr;
        if (fdes->offset) {
            ptr = fdes->bits[fdes->offset[ch - fdes->firstchar]];
        }
        else {
            int bw = (fdes->maxwidth + 15) / 16;
            ptr = fdes->bits + (ch - fdes->firstchar) * bw * fdes->height;
        }
        for (int i = 0; i < fdes->height; i++) {
            font_bits_t val = *ptr;;
            for (int j = 0; j < w; j++) {
                if ((val & 0x8000) != 0) {
                    draw_pixel_height(x + scale_y * j, y + scale_y * i, color, fb);
                }
                val <<= 1;
            }
            ptr++;
        }
    }
}

void draw_text(int x, int y, int sizeX, int sizeY, uint16_t color, font_descriptor_t* fdes, char* str, unsigned char* parlcd_mem_base, uint16_t* fb) {
    char* ch = str;
    for (int i = 0; i < strlen(str); i++) {
        draw_char(x, y, sizeY, fdes, *ch, color, fb);
        x += sizeX * char_width(fdes, *ch) + 1;
        ch++;
    }
    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (int ptr = 0; ptr < 480 * 320; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }
}
