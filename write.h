#pragma once

#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

#include "font_types.h"

unsigned short* fb;

void draw_pixel(int x, int y, uint16_t color, uint16_t* fb);

void draw_pixel_height(int x, int y, uint16_t color, uint16_t* fb);

int char_width(font_descriptor_t* fdes, int ch);

void draw_char(int x, int y, int scale_y, font_descriptor_t* fdes, char ch, uint16_t color, uint16_t* fb);

void draw_text(int x, int y, int sizeX, int sizeY, uint16_t color, font_descriptor_t* fdes, char* str, unsigned char* parlcd_mem_base, uint16_t* fb);
