#include <time.h>
#include <stdbool.h>
#include <stdio.h> 
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

void setMode(int wantKey)
{
    static struct termios old, new;
    if (!wantKey) {
        tcsetattr(STDIN_FILENO, TCSANOW, &old);
        return;
    }
    tcgetattr(STDIN_FILENO, &old);
    new = old;
    new.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &new);
}

int getKey() //select + ISSET gives us information on whether there is input to be read
{
    int c = 0;
    struct timeval tv;
    fd_set fs;
    tv.tv_usec = tv.tv_sec = 0;
    FD_ZERO(&fs);
    FD_SET(STDIN_FILENO, &fs);
    select(STDIN_FILENO + 1, &fs, 0, 0, &tv);
    if (FD_ISSET(STDIN_FILENO, &fs)) { 
        c = getchar();
        setMode(0);
    }
    return c;
}